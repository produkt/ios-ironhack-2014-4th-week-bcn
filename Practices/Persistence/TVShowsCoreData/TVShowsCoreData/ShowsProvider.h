//
//  ShowsProvider.h
//  TVShowsCoreData
//
//  Created by Daniel García on 28/10/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NSManagedObjectContext;
@interface ShowsProvider : NSObject
- (NSArray *)loadShowsInManagedObjectContext:(NSManagedObjectContext *)managedObjectContext;
@end
