//
//  ShowsProvider.m
//  TVShowsCoreData
//
//  Created by Daniel García on 28/10/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "ShowsProvider.h"
#import <CoreData/CoreData.h>
#import "ShowEntity.h"

@implementation ShowsProvider
- (NSArray *)loadShowsInManagedObjectContext:(NSManagedObjectContext *)managedObjectContext{
    NSMutableArray *shows = [NSMutableArray array];
    
    NSError *error;
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"shows" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSDictionary *JSONDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    
    for (NSDictionary *showData in [JSONDictionary valueForKey:@"shows"]) {
        ShowEntity *show = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([ShowEntity class]) inManagedObjectContext:managedObjectContext];
        show.showId = showData[@"id"];
        show.showName = showData[@"title"];
        [shows addObject:show];
    }
    
    [managedObjectContext save:&error];
    return shows;
}
@end
