//
//  ViewController.m
//  TVShowsCoreData
//
//  Created by Daniel García on 28/10/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "ViewController.h"
#import "ShowsProvider.h"
#import "ShowEntity.h"
#import "DetailViewController.h"

@interface ViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (strong,nonatomic) ShowsProvider *showsProvider;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *shows;
@end

@implementation ViewController
- (NSArray *)loadCachedShows{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([ShowEntity class])];
    fetchRequest.predicate = [NSPredicate predicateWithValue:YES];
    NSArray *shows = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    return shows;
}
- (ShowsProvider *)showsProvider{
    if (!_showsProvider) {
        _showsProvider = [[ShowsProvider alloc] init];
    }
    return _showsProvider;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.shows = [self loadCachedShows];
    if (self.shows.count == 0) {
        self.shows = [self.showsProvider loadShowsInManagedObjectContext:self.managedObjectContext];
    }
    [self saveTVShowsNamesPList];
}

- (void)saveTVShowsNamesPList{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachePath = [paths firstObject];
    NSString *plistPath = [cachePath stringByAppendingPathComponent:@"shows.plist"];
    
    
    NSMutableArray *showsNames = [NSMutableArray array];
    for (ShowEntity *show in self.shows) {
        [showsNames addObject:show.showName];
    }
    [showsNames writeToFile:plistPath atomically:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.shows.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    ShowEntity *show = [self.shows objectAtIndex:indexPath.row];
    cell.textLabel.text = show.showName;
    return cell;
}
#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ShowEntity *show = [self.shows objectAtIndex:indexPath.row];
    DetailViewController *detailVC = [[DetailViewController alloc]initWithShow:show];
    [self.navigationController pushViewController:detailVC animated:YES];    
}
@end
