//
//  NSCoderMock.h
//  Models
//
//  Created by Daniel García on 27/10/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSCoderMock : NSObject
@property (assign,nonatomic) NSUInteger numberOfCalls;
@property (assign,nonatomic) NSUInteger floatNumberOfCalls;
- (void)encodeObject:(id)objv forKey:(NSString *)key;
- (void)encodeFloat:(CGFloat)number forKey:(NSString *)key;
@end
