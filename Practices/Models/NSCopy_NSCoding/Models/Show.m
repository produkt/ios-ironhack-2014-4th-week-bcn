//
//  Product.m
//  Models
//
//  Created by Daniel García on 17/05/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "Show.h"

@implementation Show
- (NSString *)description{
    return self.showTitle;
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"showDescriptioasdfafsdn": @"description",
             @"showTitle": @"title",
             @"showId":@"id"
             };
}

@end

@implementation Show(Equality)
//- (BOOL)isEqual:(id)object{
//    if (self==object) {
//        return YES;
//    }
//    if (![object isKindOfClass:[self class]]) {
//        return NO;
//    }
//    return [self isEqualToShow:(Show *)object];
//}
- (BOOL)isEqualToShow:(Show *)show{
    return [self isEqual:show];
}
//- (NSUInteger)hash{
//    return [self.showId hash];
//}
@end

//@implementation Show(NSCopying)
//- (id)copyWithZone:(NSZone *)zone{
//    Show *showCopy = [[[self class] allocWithZone:zone] init];
//    if (showCopy) {
//        showCopy.showId = [self.showId copyWithZone:zone];
//        showCopy.showDescription = [self.showDescription copyWithZone:zone];
//        showCopy.showTitle = [self.showTitle copyWithZone:zone];
//        showCopy.showRating = self.showRating;
//    }
//    return showCopy;
//}
//@end
//
//static NSString * const showIdKey = @"showId";
//static NSString * const showDescriptionKey = @"showDescription";
//static NSString * const showTitleKey = @"showTitle";
//static NSString * const showRatingKey = @"showRating";
//
//@implementation Show(NSCoding)
//- (id)initWithCoder:(NSCoder *)aDecoder{
//    self = [super init];
//    if (self) {
//        self.showId = [aDecoder decodeObjectForKey:showIdKey];
//        self.showDescription = [aDecoder decodeObjectForKey:showDescriptionKey];
//        self.showTitle = [aDecoder decodeObjectForKey:showTitleKey];
//        self.showRating = [aDecoder decodeFloatForKey:showRatingKey];
//        
//    }
//    return self;
//}
//- (void)encodeWithCoder:(NSCoder *)aCoder{
//    [aCoder encodeObject:self.showId forKey:showIdKey];
//    [aCoder encodeObject:self.showDescription forKey:showDescriptionKey];
//    [aCoder encodeObject:self.showTitle forKey:showTitleKey];
//    [aCoder encodeFloat:self.showRating forKey:showRatingKey];
//}
//@end






