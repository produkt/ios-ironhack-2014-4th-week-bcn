//
//  TVShow.h
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 27/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface TVShow : MTLModel<MTLJSONSerializing>
@property (copy,nonatomic) NSString *showId;
@property (copy,nonatomic) NSString *showDescription;
@property (copy,nonatomic) NSString *showTitle;
@property (assign,nonatomic) CGFloat showRating;
@property (strong,nonatomic) NSString *posterImageURL;
@end
