//
//  Movie.m
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 27/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "Movie.h"

@implementation Movie
+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{
             @"movieTitle":@"title",
             @"posterURL":@"images.poster"
             };
}
@end
