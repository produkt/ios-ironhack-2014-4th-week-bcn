//
//  TVShow.m
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 27/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "TVShow.h"

@implementation TVShow
+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{
             @"showTitle":@"title",
             @"showDescription":@"overview",
             @"posterImageURL":@"images.poster"
             };
}
@end
