//
//  TVShowViewController.h
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 29/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TVShow;
@interface TVShowDetailViewController : UIViewController
- (instancetype)initWithShow:(TVShow *)show;
@end
