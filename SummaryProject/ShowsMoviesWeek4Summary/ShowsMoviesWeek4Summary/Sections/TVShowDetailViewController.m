//
//  TVShowViewController.m
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 29/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "TVShowDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "TVShow.h"

@interface TVShowDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *posterImageView;
@property (weak, nonatomic) IBOutlet UILabel *showDescriptionLabel;
@property (strong,nonatomic) TVShow *show;
@end

@implementation TVShowDetailViewController

- (instancetype)initWithShow:(TVShow *)show
{
    self = [self initWithNibName:nil bundle:nil];
    if (self) {
        _show = show;
        self.title = show.showTitle;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self drawShowView];
}

- (void)drawShowView{
    self.titleLabel.text = self.show.showTitle;
    self.showDescriptionLabel.text = self.show.showDescription;
    [self.posterImageView setImageWithURL:[NSURL URLWithString:self.show.posterImageURL] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
