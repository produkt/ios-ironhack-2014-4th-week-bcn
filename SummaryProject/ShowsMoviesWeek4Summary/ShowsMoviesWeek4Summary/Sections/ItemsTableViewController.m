//
//  ItemsTableViewController.m
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 27/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "ItemsTableViewController.h"
#import "MediaItemProtocol.h"
#import "MediaInteractor.h"

@interface ItemsTableViewController ()<UISearchBarDelegate,UITextFieldDelegate,UIScrollViewDelegate>
@property (strong,nonatomic) MediaInteractor *mediaInteractor;
@property (weak, nonatomic) IBOutlet UISearchBar *searchInput;
@property (strong,nonatomic) NSArray *items;
@property (strong,nonatomic) NSArray *originalItems;

@end

@implementation ItemsTableViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        _mediaInteractor = [[MediaInteractor alloc] init];
        self.title = @"Media Items";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.searchInput.delegate = self;
    [self loadMedia];
}

- (void)loadMedia{
    [self.mediaInteractor mediaItemsWithCompletion:^(NSArray *mediaItems) {
        self.items = mediaItems;
        [self.tableView reloadData];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id<MediaItemProtocol> item = [self.items objectAtIndex:indexPath.row];
    UITableViewCell *cell = [item.cellDrawer cellForTableView:tableView atIndexPath:indexPath];
    [item.cellDrawer drawCell:cell withItem:item];
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id<MediaItemProtocol> item = [self.items objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:[item detailViewController] animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if ([self.searchInput isFirstResponder]) {
        [self.searchInput resignFirstResponder];
    }
}
#pragma mark - UISearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (!searchText.length) {
        [self resetResults];
        return;
    }
    if (!self.originalItems) {
        self.originalItems = self.items;
    }
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(itemTitle CONTAINS[c] %@)",searchText];
    NSArray *filteredResults = [self.originalItems filteredArrayUsingPredicate:predicate];
    self.items = filteredResults;
    [self.tableView reloadData];
}

- (void)resetResults{
    if (self.originalItems) {
        self.items = self.originalItems;
        self.originalItems = nil;
        [self.tableView reloadData];
    }
}


@end
