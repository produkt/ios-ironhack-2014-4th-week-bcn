#Concurrency Troubleshooting

## IronHack iOS Bootcamp 
##![100%,inline](assets/ironhacklogo.png)
### Week 4 - Day 5


####Daniel García - Produkt
---

# Locks

## Accessing the same resource from different threads may create unexpected behaviours or bugs really hard to debug.
## We can use locks to protect critical sections of our application and synchronise the access to them.

---

# Locks

##But handling locks requires care. 
##An incorrect use of locks will produce other kind of troubles

---

# Locks

##We may be using locks without knowing
##Either *Apple* APIs or a *3rd party library* may be locking resources or dispatch queues

---

# Deadlocks

##A deadlock occurs when two threads end up waiting for the other to release a resource. 


---

# Deadlocks

*Queue A* locks *resource 1*
*Queue B* locks *resource 2*
*Queue A* tries to lock *resource 2*
*Queue B* tries to lock *resource 1*
*Queue A* keeps waiting until *Queue B* releases *resource 2*
*Queue B* keeps waiting until *Queue A* releases *resource 1*

---

# Deadlocks

## *ANYTHING* could be a resource, even a queue

---

# Deadlocks

```objectivec

dispatch_queue_t myDispatchQueue;
dispatch_async(myDispatchQueue, ^{
	/// Some code
	dispatch_sync(myDispatchQueue,^{
		/// This never reaches
	});
});

```

---

# Deadlocks

```objectivec

dispatch_queue_t myDispatchQueue;
dispatch_async(myDispatchQueue, ^{
	/// Some code
	dispatch_sync(myDispatchQueue,^{
		/// This never reaches
	});
});

```
Classic Noob Mistake : `dispatch_sync` locks the queue immediately. Trying to `dispatch_sync` from the same queue leads to a deadlock. 

---

# Deadlocks

```objectivec

dispatch_sync(dispatch_get_main_queue(),^{

});

```

Will this piece of code execute always from a different queue than the main_queue?


---

# Deadlocks

##Anytime we try to lock two different resources at the same time, it is a *'code smell'* for a possible deadlock


---

# Livelocks 

##Livelocks are very similar to a deadlock. But no thread get locked.

---

# Livelocks 

##Two people in a corridor metaphor


---

# Livelocks 

##Bank Transactions
*Queue A* withdraw 10€ from *account 1*
*Queue B* withdraw 20€ from *account 2*
*Queue A* tries to transfer 10€ to *account 2* but is locked. Refounded;
*Queue B* tries to transfer 20€ to *account 1* but is locked. Refounded;

---

# Livelocks 

##Bank Transactions
*Queue A* withdraw 10€ from *account 1*
*Queue B* withdraw 20€ from *account 2*
*Queue A* tries to transfer 10€ to *account 2* but is locked. Refounded;
*Queue B* tries to transfer 20€ to *account 1* but is locked. Refounded;
*Queue A* withdraw 10€ from *account 1*
*Queue B* withdraw 20€ from *account 2*
*Queue A* tries to transfer 10€ to *account 2* but is locked. Refounded;
*Queue B* tries to transfer 20€ to *account 1* but is locked. Refounded;
...

---

# Dealing with it 
---

## Best way to deal with *deadlocks* and *livelocks* is to avoid locking two resources at the same time

---

#@synchronized

##Directive that allow us to easily `synchronise` access to a critical section of our application

---

#@synchronized
```objectivec
- (void)myMethod:(id)anObj
{
    @synchronized(anObj)
    {
        // Everything between the braces is protected 
		// by the @synchronized directive.
    }
}
```

---

#@synchronized
```objectivec
- (void)myMethod
{
    @synchronized(self)
    {
        // Everything between the braces is protected 
		// by the @synchronized directive.
    }
}
```

---

#@synchronized

##`@synchronized` trade-off : Internally uses exception handling to unlock the resource. We *must* enable exception handling in our project.

##Overhead

---

#NSLock

##High Level API Class for mutex lock handling

---

#NSLock 

```objectivec
@property(strong,nonatomic) NSLock *criticalResourceLock;

- (NSLock *)criticalResourceLock{
	if(!_criticalResourceLock){
		_criticalResourceLock=[[NSLock alloc] init];
	}
	return _criticalResourceLock;
}

```

---

#NSLock 

```objectivec
BOOL moreToDo = YES;
NSLock *theLock = self.criticalResourceLock;
...
while (![theLock tryLock]){};

/* Do stuff */
[theLock unlock];
```
---

#NSLock 

#![inline,150%](assets/NSLockwarn.png)

## *Always* release your lock the same thread that you locked it


